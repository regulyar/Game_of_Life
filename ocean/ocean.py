from cells.cell import EmptyCell, Rock, Fish, Crayfish
from enum import Enum


class OceanInterpr:
    types = {str(EmptyCell()): EmptyCell,
             str(Rock()): Rock,
             str(Fish()): Fish,
             str(Crayfish()): Crayfish}

    @staticmethod
    def interpr(c):
        if OceanInterpr.types.get(c) == None:
            raise ValueError('No such symbol ' + c)
        return OceanInterpr.types[c]()


class GameType(Enum):
    RECTANGLE = 0
    TORUS = 1


class Ocean:
    def __init__(self, startCondition, gameType=GameType.RECTANGLE, width=1, height=1):
        self.gameType = gameType
        self.width = width
        self.height = height
        self.cells = [[None] * height for i in range(width)]
        for i in range(self.width):
            for j in range(self.height):
                self.cells[i][j] = OceanInterpr.interpr(startCondition[i][j])

    def getNeighbour(self, x, y, xShift, yShift):
        if self.gameType == GameType.TORUS:
            return self.cells[(x + xShift) % self.width][(y + yShift) % self.height]
        if self.gameType == GameType.RECTANGLE:
            if x + xShift == self.width or x + xShift == -1 or \
                    y + yShift == self.height or y + yShift == -1:
                return EmptyCell()
            else:
                return self.cells[x + xShift][y + yShift]

    def getNeighbours(self, x, y):
        neighbours = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                neighbours.append(self.getNeighbour(x, y, i, j))
        return neighbours

    def nextGeneration(self):
        tmpCondition = [[None] * self.height for i in range(self.width)]
        for i in range(self.width):
            for j in range(self.height):
                tmpCondition[i][j] = self.cells[i][j].update(self.getNeighbours(i, j))
        for i in range(self.width):
            for j in range(self.height):
                self.cells[i][j] = tmpCondition[i][j]

    def nextGenerations(self, n):
        for i in range(n):
            self.nextGeneration()

    def __str__(self):
        oceanString = ''
        for i in range(self.width):
            for j in range(self.height):
                oceanString += str(self.cells[i][j])
            oceanString += '\n'
        return oceanString
