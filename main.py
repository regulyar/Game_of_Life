import game_process.run
import argparse


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--src', nargs='?', const='stdin', default='stdin',
                        help='Use SRC to input, stdin means standard input')
    parser.add_argument('--dst', nargs='?', const='stdout', default='stdout',
                        help='Use DST to output, stdout means standard output')

    game_process.run.runGame(parser.parse_args().src, parser.parse_args().dst)


if __name__ == '__main__':
    main()
