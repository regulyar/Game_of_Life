from ocean.ocean import GameType, Ocean
import sys


def readData(src):
    if src == 'stdin':
        source = sys.stdin
    else:
        source = open(src, 'r')
    with source as inStream:
        generations = int(inStream.readline().split()[0])
        width, height = map(int, inStream.readline().split())
        oceanMap = [[None] * height for i in range(width)]
        for i in range(width):
            oceanMap[i] = list(inStream.readline().strip())
        return [Ocean(oceanMap, GameType.RECTANGLE, width, height), generations]


def handleData(data):
    data[0].nextGenerations(data[1])


def writeData(dst, data):
    if dst == 'stdout':
        dest = sys.stdout
    else:
        dest = open(dst, 'w')
    with dest as outFile:
        outFile.write(str(data))


def runGame(src, dst):
    data = readData(src)
    handleData(data)
    writeData(dst, data[0])
