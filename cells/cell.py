from enum import Enum


class CellType(Enum):
    EMPTY = 0
    ROCK = 1
    FISH = 2
    CRAYFISH = 3


class Cell:
    class BornChecker:
        needNeighbours = {CellType.FISH: 3, CellType.CRAYFISH: 3}

        def fish(self, neighbours):
            fishCounter = 0
            for i in neighbours:
                if i.type == CellType.FISH:
                    fishCounter += 1
            if fishCounter == self.needNeighbours[CellType.FISH]:
                return True
            else:
                return False

        def crayfish(self, neighbours):
            crayfishCounter = 0
            for i in neighbours:
                if i.type == CellType.CRAYFISH:
                    crayfishCounter += 1
            if crayfishCounter == self.needNeighbours[CellType.CRAYFISH]:
                return True
            else:
                return False

    class DieChecker:
        needMinNeighbours = {CellType.FISH: 2, CellType.CRAYFISH: 2}
        needMaxNeighbours = {CellType.FISH: 3, CellType.CRAYFISH: 3}

        def fish(self, neighbours):
            fishCounter = 0
            for i in neighbours:
                if i.type == CellType.FISH:
                    fishCounter += 1
            if fishCounter < self.needMinNeighbours[CellType.FISH] or \
                    fishCounter > self.needMaxNeighbours[CellType.FISH]:
                return True
            else:
                return False

        def crayfish(self, neighbours):
            crayfishCounter = 0
            for i in neighbours:
                if i.type == CellType.CRAYFISH:
                    crayfishCounter += 1
            if crayfishCounter < self.needMinNeighbours[CellType.CRAYFISH] or \
                    crayfishCounter > self.needMaxNeighbours[CellType.CRAYFISH]:
                return True
            else:
                return False

    def __init__(self):
        self.bornChecker = self.BornChecker
        self.dieChecker = self.DieChecker()


class Fish(Cell):
    type = CellType.FISH

    def __str__(self):
        return 'F'

    def update(self, neighboursList):
        if self.dieChecker.fish(neighboursList):
            return EmptyCell()
        else:
            return Fish()


class Crayfish(Cell):
    type = CellType.CRAYFISH

    def __str__(self):
        return 'C'

    def update(self, neighbours):
        if self.dieChecker.crayfish(neighbours):
            return EmptyCell()
        else:
            return Crayfish()


class EmptyCell(Cell):
    type = CellType.EMPTY

    def __str__(self):
        return '.'

    def __init__(self):
        super().__init__()
        self.bornCondition = {CellType.FISH: self.bornChecker.fish,
                              CellType.CRAYFISH: self.bornChecker.crayfish}
        self.newCreature = {CellType.FISH: Fish,
                            CellType.CRAYFISH: Crayfish}

    def update(self, neighbours):
        priority = (CellType.FISH, CellType.CRAYFISH)
        for t in priority:
            if self.bornCondition[t](self.bornChecker, neighbours):
                return self.newCreature[t]()
        return EmptyCell()


class Rock(Cell):
    type = CellType.ROCK

    def __str__(self):
        return '#'

    def update(self, neighbours):
        return Rock()
