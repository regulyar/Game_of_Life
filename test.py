import unittest
from ocean.ocean import GameType, Ocean
from game_process.input import InputType, Input


def getListFromString(str):
    lst = list(map(list, str.split()))
    return lst


def getListFromMap(oceanS):
    lst = [[None] * oceanS.ySize for i in range(oceanS.xSize)]
    for i in range(oceanS.xSize):
        for j in range(oceanS.ySize):
            lst[i][j] = oceanS.cells[i][j].str
    return lst


class TestOcean(unittest.TestCase):

    def prepare(self, inputFile, outputFile):
        input = Input(InputType.FILEIN, inputFile)
        input.read()
        oceanS = Ocean(input.oceanMap, GameType.RECTANGLE, input.xSize, input.ySize)
        oceanS.nextGenerations(input.generations)
        self.lst1 = getListFromMap(oceanS)
        f = open(outputFile, 'r')
        self.lst2 = getListFromString(f.read())
        f.close()

    def checkEq(self):
        self.assertEqual(len(self.lst1), len(self.lst2))
        for i in range(len(self.lst2)):
            self.assertEqual(len(self.lst1[i]), len(self.lst2[i]))
            for j in range(len(self.lst2[i])):
                self.assertEqual(self.lst1[i][j], self.lst2[i][j])

    def test_largeTest(self):
        for i in range(100):
            self.prepare('tests1/test' + str(i) + '_input.txt',
                         'tests1/test' + str(i) + '_output.txt')
            self.checkEq()
            print('TEST ' + str(i) + ' PASSED')


def main():
    unittest.main()


if __name__ == '__main__':
    main()
